package com.celfocus.celfocus_aem_training.core;


public interface RepositoryService {
    public String getRepositoryName();
}