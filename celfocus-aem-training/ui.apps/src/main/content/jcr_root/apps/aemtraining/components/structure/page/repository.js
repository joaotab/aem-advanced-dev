use( function() {
	var Repository = Packages.com.celfocus.celfocus_aem_training.core.RepositoryService;
	
	return {
		repoName: sling.getService(Repository).getRepositoryName()
	};
});